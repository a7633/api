//fRe5pvzuI9Fm0o8d password
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const userRoute = require("./routes/user")
const productRoute = require("./routes/product")
const cartRoute = require("./routes/cart")
const orderRoute = require("./routes/order")
const authRoute = require("./routes/auth")
const stripeRoute = require("./routes/stripe")
const cors = require("cors");

dotenv.config();

app.use(express.json());
app.use(cors());

mongoose
.connect(process.env.MONGO_URL)
.then(()=>console.log("DB connection is successfull"))
.catch((err)=>{
    console.log(err);
});

app.use("/api/auth", authRoute);
app.use("/api/users", userRoute);
app.use("/api/products", productRoute);
app.use("/api/carts", cartRoute);
app.use("/api/orders", orderRoute);
app.use("/api/ckeckout", stripeRoute);

app.listen(process.env.PORT || 5000, ()=>{
    console.log("backend server is running")
});